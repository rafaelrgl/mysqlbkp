# This script is created by Rafael Lima. Please do not reamove this referal from the script.
# and if this helped you a lot, please come visit my website and give one like :)
# Rafael Lima's website: www.rafaelrglima.com
# Bitbucket Project:

# This script intent is to make the backup of mysql easy and also make backup of large databases possible with mysqldump
# when the table is too large is necessar to split the backup in small batches, this one of the rules we apply here, if
# the table has more than 250000 rows, we will split these into different files. We also split the backup into different
# files, here is the list of files we will create:

# databasename_create.sql  - this file is just to create the database structure without data.
# databasename_tablename.sql - this is the file to create the structure of just that table
# databasename_tablename_data.zip - this is an zip file with all the data for this table. if this table is larger than 250.000 rows, we will have this file like this databasename_tablename_data_1.zip and so on
#                                   I choose to zip those files because we will save space in harddrive, but if you unzip this you will find the .sql file inside.
# databasename_restore.sh - this sh script will execute all tasks to restore the database, if you did not set to store mysql login information inside this file, you just need to pass the login as you would in mysql connection
#                           example: databasename_restore.sh -u root -p 123456 -h localhost
