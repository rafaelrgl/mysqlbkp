#!/bin/bash
# This script is created by Rafael Lima. Please do not reamove this referal from the script.
# and if this helped you a lot, please come visit my website and give one like :)
# Rafael Lima's website: www.rafaelrglima.com
# Bitbucket Project:

# This script intent is to make the backup of mysql easy, storage them in small files and also make backup of large databases possible with mysqldump
# when the table is too large is necessary to split the backup in small batches, this is one of the rules we apply here, if
# the table has more than 250000 rows, we will split these into different files. We also split the backup into different
# files, here is the list of files we will create:

# databasename_create.sql  - this file is just to create the database structure without data.
# databasename_tablename.sql - this is the file to create the structure of just that table
# databasename_tablename_data.sql or .zip - this is an zip file with all the data for this table. if this table is larger than 250.000 rows, we will have this file like this databasename_tablename_data_1.zip and so on
#                                   I choose to zip those files because we will save space in harddrive, but if you unzip this you will find the .sql file inside.
# databasename_restore.sh - this sh script will execute all tasks to restore the database, if you did not set to store mysql login information inside this file, you just need to pass the login as you would in mysql connection
#                           example: databasename_restore.sh -u root -p 123456 -h localhost


#tar -zcvf owncloud.tar.gz owncloud
#tar -xvzf digisoft.tar.gz


#=======================================================================
#this commands here is just a hint to compress the files for windows too
#tar cvf - folderToCompress | gzip > compressFileName
#To expand the archive:
#zcat compressFileName | tar xvf -
#=======================================================================

# if you want to set these variables here inside the script so next time you run the backup it will have them store, please changes
# them according to each one.

# Here is the databases you want to backup, if you want more then one, put comma separating their names like this: database1,database2,database3
DATABASES=""

# Here is the mysql configuration where the database are. localhost is the default, but you can change this if you need to.
MYSQL_FROM_HOST="localhost"
MYSQL_FROM_USER=""
MYSQL_FROM_PASSWORD=""

# if you want the backup to transfer the database from one server to another, please put the mysql configuration below:
MYSQL_TO_HOST=""
MYSQL_TO_USER=""
MYSQL_TO_PASSWORD=""

#these are the ftp setting to transfer your database to one ftp
FTP_HOST=""
FTP_USER=""
FTP_PASSWORD=""

#these are the sftp setting to transfer your database to one sftp
SFTP_HOST=""
SFTP_USER=""
SFTP_PASSWORD=""

# this is the backup dir where you want the backup to be store,
BACKUP_DIR=""

# this is set to 1 as default, because the default is to create the backup files locally, but if you want to just transfer from one server to another set this to 0
BACKUP_LOCAL=1

# this is set to 0 as default, because the default is not to write the login information inside the restore.sh script,
# but if you need this or just want to execute next time without passing password and login, just set to 1
BACKUP_MYSQL_CONFIG=0





# from here below, do not change anything if you do not know what you are doing.
DATE=`/bin/date '+%y%m%d_%H%M%S'`
help()
{
   echo ""
   echo ""
   echo "============================================================="
   echo "          This is script is made by Rafael Lima              "
   echo "  check this site for more information: www.rafaelrglima.com "
   echo "============================================================="
   echo "                                                             "
   echo "                                                             "
   echo "Here is some examples how to use this"
   echo ""
   echo "usage: sh mysqlbkp.sh -p /var/bkpdir -d databasename -fh localhost -fu root -fp 123456"
   echo ""
   echo ""
   echo "usage: sh mysqlbkp.sh -p /var/bkpdir -d databasename -fh localhost -fu root -fp 123456 -th localhost -tu root -tp 123456"
   echo ""
   echo "usage: sh mysqlbkp.sh -p /var/bkpdir -d databasename1,databasename2,databasename3 -i "
   echo ""
   echo ""
}

##### Main
while [ "$1" != "" ]; do
    case $1 in
        -d | --databases )     shift
                               DATABASES=$1
                               ;;

        -p | --filepath )      shift
                               BACKUP_DIR=$1
                               ;;

        -fh | --fromhost )     shift
                               MYSQL_FROM_HOST=$1
                               ;;

        -fu | --fromuser )     shift
                               MYSQL_FROM_USER=$1
                               ;;

        -fp | --frompassword ) shift
                               MYSQL_FROM_PASSWORD=$1
                               ;;

        -th | --tohost )       shift
                               MYSQL_TO_HOST=$1
                               ;;

        -tu | --touser )       shift
                               MYSQL_TO_USER=$1
                               ;;

        -tp | --topassword )   shift
                               MYSQL_TO_PASSWORD=$1
                               ;;
       -bl | --backuplocal )   shift
                              BACKUP_LOCAL=$1
                              ;;

        -h | --help )          help
                               exit
                               ;;

        * )                    echo ""
                               echo ""
                               echo "$1 is not an option for this command, please check the help below"
                               echo ""
                               help
                               exit 1
    esac
    shift
done
if [ "$DATABASES" = "" ]; then
  echo "ERROR: You have to inform at least one database like this:  -d databasename1"
  exit
fi
if [ "$BACKUP_DIR" = "" ]; then
  echo "ERROR: You did not inform the path where the backup will be store like this:  -p /var/bkp"
  exit
fi
if [ "$MYSQL_FROM_USER" = "" ]; then
  echo "ERROR: You have to inform the mysql user like this:  -fu mysqlusername"
  exit
fi
if [ "$MYSQL_FROM_PASSWORD" = "" ]; then
  echo "ERROR: You have to inform the mysql password like this:  -fp mysqlpassword"
  exit
fi
if [ "$BACKUP_LOCAL" != "1" ] && ["$BACKUP_LOCAL" != "0"]; then
  echo "ERROR: The BACKUP_LOCAL only accepts 1 or 0 and per default we backup this local."
  exit
fi

for DB in $DATABASES
do
  if [ "$BACKUP_LOCAL" = "1" ]; then
        BACKUP_PATH=${BACKUP_DIR}/${DB}/$(date +'%Y_%m_%d_%H_%m_%s')
        if [ ! -d "$BACKUP_PATH" ]; then
          mkdir -p -v ${BACKUP_PATH}
        else
          echo "There is already one directory inside this folder $BACKUP_PATH"
          exit
        fi
        echo "=========================================="
        echo                 ${DB}
        echo "=========================================="
        mysqldump -d -h${MYSQL_FROM_HOST} -u${MYSQL_FROM_USER} -p${MYSQL_FROM_PASSWORD} -B --events --routines --triggers ${DB} > ${BACKUP_PATH}/${DB}.sql
        printf "\n mysql -h${MYSQL_FROM_HOST} -u${MYSQL_FROM_USER} -p${MYSQL_FROM_PASSWORD} < ${DB}.sql;" >> ${BACKUP_PATH}/${DB}_restore.sh

        for TABLE in `mysql -h$MYSQL_FROM_HOST -u$MYSQL_FROM_USER -p$MYSQL_FROM_PASSWORD $DB -e 'show tables' | egrep -v 'Tables_in_' `; do
            TABLENAME=$(echo $TABLE|awk '{ printf "%s", $0 }')

            printf "\n gunzip < ${DB}_${TABLENAME}_data.zip | mysql -h${MYSQL_FROM_HOST} -u${MYSQL_FROM_USER} -p${MYSQL_FROM_PASSWORD} ${DB}" >> ${BACKUP_PATH}/${DB}_restore.sh

            echo Dumping $TABLENAME structure.
            mysqldump -h${MYSQL_FROM_HOST} -u${MYSQL_FROM_USER} -p${MYSQL_FROM_PASSWORD} -d --triggers ${DB} ${TABLENAME} > ${BACKUP_PATH}/${DB}_${TABLENAME}.sql
            echo Dumping $TABLENAME data.
            mysqldump -h${MYSQL_FROM_HOST} -u${MYSQL_FROM_USER} -p${MYSQL_FROM_PASSWORD} --no-create-info -q --order-by-primary ${DB} ${TABLENAME} | gzip -r > ${BACKUP_PATH}/${DB}_${TABLENAME}_data.zip
            if [ "$MYSQL_TO_HOST" != "" ] && [ "$MYSQL_TO_USER" != "" ] && [ "$MYSQL_TO_PASSWORD" != "" ]; then
              echo "Transfering table from zip arquive to the target mysql server."
              gunzip < ${DB}_${TABLENAME}_data.zip | mysqldump -h${MYSQL_TO_HOST} -u${MYSQL_TO_USER} -p${MYSQL_TO_PASSWORD} ${DB} ${TABLENAME}
            fi
        done
        echo "done with " $DB
  else
    if [ "$MYSQL_TO_HOST" = "" ]; then
      echo "ERROR: You have to inform the mysql targe host like this:  -tp mysqltargethost"
      exit
    fi
    if [ "$MYSQL_TO_USER" = "" ]; then
      echo "ERROR: You have to inform the mysql target user like this:  -fp mysqltargetuser"
      exit
    fi
    if [ "$MYSQL_TO_PASSWORD" = "" ]; then
      echo "ERROR: You have to inform the mysql target password like this:  -fp mysqltargetpassword"
      exit
    fi
    for TABLE in `mysql -h$MYSQL_FROM_HOST -u$MYSQL_FROM_USER -p$MYSQL_FROM_PASSWORD $DB -e 'show tables' | egrep -v 'Tables_in_' `; do
        TABLENAME=$(echo $TABLE|awk '{ printf "%s", $0 }')
        echo Transering $TABLENAME.
        mysqldump -h${MYSQL_FROM_HOST} -u${MYSQL_FROM_USER} -p${MYSQL_FROM_PASSWORD} ${DB} ${TABLENAME} | mysqldump -h${MYSQL_TO_HOST} -u${MYSQL_TO_USER} -p${MYSQL_TO_PASSWORD} ${DB} ${TABLENAME}
    done
  fi
done
echo
echo
echo "Your backup is store at: $BACKUP_PATH"
echo
echo "=========================================="
echo "                   done                   "
echo "=========================================="
echo
